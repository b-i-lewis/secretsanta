<?php

namespace App\Console;

use App\Http\Controllers\RandomController;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {

            $santa = new RandomController();
            $santa->assignSantas();

        })->cron('0 12 5 11 2018');

        $schedule->call( function () {

            $santa = new RandomController();
            $santa->countdown();

        })->dailyAt('09:00')->when( function () {

            $targetDate = Carbon::createFromFormat('Y-m-d H:i:s', config('app.targetDate'));
            $now = new Carbon();
            return $now < $targetDate;

        });
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
