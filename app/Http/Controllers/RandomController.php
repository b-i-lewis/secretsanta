<?php

namespace App\Http\Controllers;

use App\Contestant;
use App\Http\Requests\StoreContestant;
use Bogardo\Mailgun\Facades\Mailgun;
use Carbon\Carbon;
use GuzzleHttp\Client;

class RandomController extends Controller
{
    public function index()
    {
        return view('welcome', [
            'contestants' => Contestant::all()
        ]);
    }

    public function create(StoreContestant $req)
    {
        $contestant = new Contestant();
        $contestant->name = $req->post('name');
        $contestant->email = $req->post('email');
        $contestant->save();

        return view('welcome', [
            'success' => true,
            'contestants' => Contestant::all()
        ]);
    }

    public function simulate()
    {
        $contestants = Contestant::all();
        $result = $this->draw( $contestants );
        while ( $result['badDraw'] ) {
            $result = $this->draw( $contestants );
        }
        return '<pre>' . json_encode( $result['contestants'], JSON_PRETTY_PRINT ) . '</pre>';
    }

    public function email( Contestant $contestant )
    {
        if( empty( $contestant->match ) ){
            throw new \InvalidArgumentException( 'A contestant did nt recieve a match');
        }
        Mailgun::raw('Hello there ' . $contestant->name .
            ', if that is your real name.<br /><br />You have been assigned ' . $contestant->match . ' for the secret santa conspiracy, but remember Ben should really get a present from everyone.<br /><br />We all die alone.',
            function ( $msg ) use ( $contestant ) {
                $msg->subject('Your Secret Santa Has Been Assigned - For Reels this time')->to( $contestant->email, $contestant->name );
            } );

    }

    public function assignSantas()
    {
        $contestants = Contestant::all();
        $result = $this->draw( $contestants, false );
        while ( $result['badDraw'] ) {
            $result = $this->draw( $contestants, false );
        }
        $contestants->each( function ( $contestant ) {
            $this->email( $contestant );
        });

    }

    public function countdown()
    {
       $targetDate = Carbon::createFromFormat('Y-m-d H:i:s', config('app.targetDate'));
       $now = new Carbon();
       $countdown = $targetDate->diffInDays( $now ) + 1 ;

       $contestants = Contestant::all();
       $contestants->each( function ( $contestant ) use ( $countdown ) {
           Mailgun::raw('Hello there ' . $contestant->name .', <br /><br /> The secret santa draw will happen in ' . $countdown . ' days.',
               function ( $msg ) use ( $contestant ) {
                   $msg->subject('Secret Santa Countdown')->to( $contestant->email, $contestant->name );
               } );
       });
    }

    private function draw( $contestants ,$cleanEmail = true )
    {
        $ids = $contestants->pluck('id');
        $contestantNames = $contestants->pluck('name','id')->toArray();

        $client = new Client();
        $payload = [
            'jsonrpc' => 2.0,
            'method' => 'generateIntegers',
            'params' => [
                'apiKey' => '30ed7f75-3c8a-478b-acf7-2abbc63f5905',
                'n' => $ids->max(),
                'min' => $ids->min(),
                'max' => $ids->max(),
                'replacement' => false, // forces unique responses
            ],
            'id' => 1,
        ];
        $response = $client->request('POST', 'https://api.random.org/json-rpc/1/invoke',
            [
                'body' => json_encode( $payload ),
            ]);
        $result = json_decode( $response->getBody() );
        if( !isset( $result->result->random->data ) ){
            return [ 'API Error' => $result ];
        }

        $results = collect($result->result->random->data);
        $results = $results->intersect($ids)->values()->toArray();

        $lastId = $ids->max();
        $badDraw = false;
        foreach ( $contestants as $contestant )
        {
            if( $cleanEmail === true ){
                unset( $contestant->email );
            }
            foreach ( $results as $k => $result )
            {
                if( $result !== $contestant->id )
                {
                    $contestant->match = $contestantNames[ $result ];
                    unset( $results[ $k ] );
                    break;
                }
                if( $contestant->id === $lastId && $result === $contestant->id )
                {
                    $badDraw = true;
                }
            }
        }
        return [
            'badDraw' => $badDraw,
            'contestants' => $contestants,
        ];
    }
}