<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contestant extends Model
{
    public function getHiddenEmail()
    {
        $exp = explode('@', $this->email, 2);
        return $exp[0] . '@' . str_repeat('*', strlen( $exp[1] ) );
    }
}
