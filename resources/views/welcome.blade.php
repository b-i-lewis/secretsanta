<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>secretSanta</title>

        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h1>Secret Santa</h1>
                </div>
                <div class="col-md-6 col-md-offset-3">
                    <p>This is a secret santa implementation that will use a <a href="//api.random.org/json-rpc/1/basic">public API (random.org)</a> to fairly assign santa's with the group.</p>
                    <h3>So KATE cant cheat</h3>
                    <p>When all the names are all collected an email will be sent with your assigned recipient</p>
                    <p>The current end time is 5/11/2018 at 12:00</p>
                </div>
                <div class="form-container col-md-6 col-md-offset-3">
                    @if( !empty( $errors->messages() ) )
                        <h4>Errors:</h4>
                        <pre>{{json_encode( $errors->messages(), JSON_PRETTY_PRINT )}}</pre>
                    @endif
                    <form method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input required="required" id="name" name="name" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input required="required" id="email" name="email" class="form-control" />
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
                <div class="col-md-6 col-md-offset-3">
                    <h3 class="title">Current Contestants</h3>
                    @if( $contestants->count() == 0 )
                        <h5> There are currently no contestants</h5>
                    @else
                        <a class="btn btn-default" style="margin: 5px 5px 10px 5px;" href="/simulate" role="button">Simulate Assignment</a>
                        <ul>
                            @foreach( $contestants as $contestant )
                                <li>{{ $contestant->name }} - {{ $contestant->getHiddenEmail() }}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </body>
</html>
